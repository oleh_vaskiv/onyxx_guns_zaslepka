var slider;

slider = $('.logos').bxSlider({
    auto: true,
    pager: false,
    speed: 1500,
    pause: 1500,
    minSlides: 1,
    maxSlides: 2,
    slideWidth: 300,
    slideMargin: 0,
    moveSlides: 1,
    controls: false,
    onSliderResize: reloadBX1,
    onSliderLoad: function () {
        $(".logos").css("visibility", "visible");
    }
});

$(window).on('resize load ready orientationchange change', function() {
    var width = $('.container').width();
    if (width > 400) {
        slider.destroySlider();
        $(".logos").css("visibility", "visible");
    } else {
        slider.reloadSlider();
    }
});


/*
Resize Callback 
*/ // Stores the current slide index.
function reloadBX1(idx) {
  localStorage.cache = idx;
  // Reloads slider, 
  ///goes back to the slide it was on before resize,
  ///removes the stored index.
  function inner(idx) {
    setTimeout(slider.reloadSlider, 0);
    var current = parseInt(idx, 10);
    slider.goToSlide(current);
    slider.startAuto();
    localStorage.removeItem("cache");
  }
}

